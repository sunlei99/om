package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-23
 */
@Data
@TableName("member_coupon")
public class MemberCoupon implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增
     */
    @TableId
    private Long id;
    /**
     * 会员id
     */
    private Long memberId;
    /**
     * 优惠券名称
     */
    private String couponName;
    /**
     * 优惠券类型：1-满减券 2-商品券 3-赠送券 4-新人券 5-通用券
     */
    private Integer couponType;
    /**
     * 优惠券面值
     */
    private Integer faceValue;
    /**
     * 面值条件（为空表示通用型，不为空表示满减型）
     */
    private Integer faceValueDemand;
    /**
     * 领取时间
     */
    private Date receiveTime;
    /**
     * 状态：默认未使用/已过期 0、1-已使用
     */
    private Integer isUsed;
    /**
     * 使用时间
     */
    private Date usedTime;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;

}
