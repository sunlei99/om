package com.zero2oneit.mall.member.controller;

import com.zero2oneit.mall.common.bean.member.MemberRetail;
import com.zero2oneit.mall.common.query.member.MemberRetailQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zero2oneit.mall.member.service.MemberRetailService;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-12
 */
@RestController
@RequestMapping("/admin/member/retail")
public class MemberRetailController {

    @Autowired
    private MemberRetailService retailService;

    /**
     * 查询分销列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody MemberRetailQueryObject qo){
        return retailService.pageList(qo);
    }

    /**
     * 添加或编辑分销信息
     * @param retail
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody MemberRetail retail){
        retailService.saveOrUpdate(retail);
        return R.ok();
    }

}
